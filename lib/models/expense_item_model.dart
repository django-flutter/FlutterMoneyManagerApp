class ExpenseItem {
  int id;
  String name;
  String img;
  ExpenseItem({this.img, this.name, this.id});
}

List expenses = [
  ExpenseItem(img: 'assets/categories/expense/food.png', name: '用餐', id: 1),
  ExpenseItem(img: 'assets/categories/expense/waimai.png', name: '外卖', id: 2),
  ExpenseItem(img: 'assets/categories/expense/clothing.png', name: '衣服', id: 3),
  ExpenseItem(img: 'assets/categories/expense/wuye.png', name: '物业', id: 4),
  ExpenseItem(img: 'assets/categories/expense/shuidian.png', name: '水电', id: 5),
  ExpenseItem(img: 'assets/categories/expense/rent.png', name: '房租', id: 6),
  ExpenseItem(img: 'assets/categories/expense/fangdai.png', name: '房贷', id: 7),
  ExpenseItem(img: 'assets/categories/expense/bus.png',name: '交通',id: 8),
  ExpenseItem(img: 'assets/categories/expense/baby.png', name: '小孩', id: 9),
  ExpenseItem(img: 'assets/categories/expense/salon.png', name: '美业', id: 10),
  ExpenseItem(img: 'assets/categories/expense/shopping.png', name: '购物', id: 11),
  ExpenseItem(img: 'assets/categories/expense/jiayong.png', name: '家用', id: 12),
  ExpenseItem(img: 'assets/categories/expense/jiaju.png', name: '家居', id: 13),
  ExpenseItem(img: 'assets/categories/expense/hongbao.png', name: '红包', id: 14),
  ExpenseItem(img: 'assets/categories/expense/gifts.png', name: '礼物', id: 15),
  ExpenseItem(img: 'assets/categories/expense/mobile.png', name: '手机', id: 16),
  ExpenseItem(img: 'assets/categories/expense/fuwu.png', name: '服务器', id: 17),
  ExpenseItem(img: 'assets/categories/expense/daikuan.png', name: '借贷', id: 18),
  ExpenseItem(img: 'assets/categories/expense/drinks.png', name: '饮料', id: 19),
  ExpenseItem(img: 'assets/categories/expense/friends.png', name: '朋友', id: 20),
  ExpenseItem(img: 'assets/categories/expense/books.png', name: '书籍', id: 21),
  ExpenseItem(img: 'assets/categories/expense/education.png', name: '学习', id: 22),
  ExpenseItem(img: 'assets/categories/expense/electronics.png',name: '数码',id: 23),
  ExpenseItem(img: 'assets/categories/expense/entertainment.png',name: '娱乐',id: 24),
  ExpenseItem(img: 'assets/categories/expense/automobile.png',name: '汽车',id: 25),
  ExpenseItem(img: 'assets/categories/expense/groceries.png',name: '杂货',id: 26),
  ExpenseItem(img: 'assets/categories/expense/health.png', name: '健康', id: 27),
  ExpenseItem(img: 'assets/categories/expense/hobbies.png', name: '爱好', id: 28),
  ExpenseItem(img: 'assets/categories/expense/investments.png',name: '投资',id: 29),
  ExpenseItem(img: 'assets/categories/expense/laundry.png', name: '洗衣', id: 30),
  ExpenseItem(img: 'assets/categories/expense/office.png', name: '办公', id: 31),
  ExpenseItem(img: 'assets/categories/expense/pets.png', name: '宠物', id: 32),
  ExpenseItem(img: 'assets/categories/expense/tax.png', name: '纳税', id: 33),
  ExpenseItem(img: 'assets/categories/expense/utilities.png',name: '公用',id: 34),
  ExpenseItem(img: 'assets/categories/expense/charity.png', name: '公益', id: 35),
  ExpenseItem(img: 'assets/categories/expense/others.png', name: '其他', id: 36),

];
