class IncomeItem {
  int id;
  String name;
  String img;
  IncomeItem({this.img, this.name, this.id});
}

List incomes = [
  IncomeItem(img: 'assets/categories/income/salary.png', name: '薪水', id: 1),
  IncomeItem(img: 'assets/categories/income/freelance.png', name: '副业', id: 2),
  IncomeItem(img: 'assets/categories/income/awards.png', name: '奖金', id: 3),
  IncomeItem(img: 'assets/categories/income/bonus.png', name: '分红', id: 4),
  IncomeItem(img: 'assets/categories/income/grants.png', name: '赠款', id: 5),
  IncomeItem(img: 'assets/categories/income/interest.png', name: '利息', id: 6),
  IncomeItem(img: 'assets/categories/income/jiekuan.png', name: '借入', id: 7),
  IncomeItem(img: 'assets/categories/income/huankuan.png', name: '还入', id: 8),
  IncomeItem(img: 'assets/categories/income/investments.png',name: '投资',id: 9),
  IncomeItem(img: 'assets/categories/income/lottery.png', name: '彩票', id: 10),
  IncomeItem(img: 'assets/categories/income/refunds.png', name: '退款', id: 11),
  IncomeItem(img: 'assets/categories/income/rent.png', name: '租金', id: 12),
  IncomeItem(img: 'assets/categories/income/sale.png', name: '出售', id: 13),
  IncomeItem(img: 'assets/categories/income/others.png', name: '其他', id: 14),
];
