import 'package:flutter/material.dart';
import 'package:money_manager/models/main_data_model.dart';
import 'package:money_manager/models/transaction_model.dart';
import 'package:money_manager/utils/constants.dart';
import 'package:money_manager/helpers/db_helper.dart';

class TransactionDetailScreen extends StatefulWidget {
  static const String id = 'transaction_detail_screen';
  final Transaction t;
  final Function refresh;
  TransactionDetailScreen({this.t, this.refresh});
  @override
  _TransactionDetailScreenState createState() =>
      _TransactionDetailScreenState();
}

class _TransactionDetailScreenState extends State<TransactionDetailScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        backgroundColor: kBackColor.withOpacity(0.15),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          actions: [
            IconButton(
              icon: Icon(
                Icons.delete_outline_rounded,
                color: kBackColor,
              ),
              onPressed: () {
                showDeleteDialog();
              },
            ),
          ],
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: kBackColor,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          centerTitle: true,
          title: Text(
            "详情",
            style: TextStyle(
              color: kBackColor,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height * 0.50,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                    color: kBackColor.withOpacity(0.4),
                  ),
                  color: Colors.white,
                ),
                child: Center(
                  child: Column(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              child: Center(
                                child: Container(
                                  height: 35.0,
                                  width: 35.0,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(widget.t.imgSrc),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                widget.t.name,
                                style: TextStyle(
                                  color: kBackColor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18.0,
                                ),
                              ),
                              flex: 2,
                            ),
                          ],
                        ),
                      ),
                      Divider(),
                      myDetailRow(
                        title: "类别",
                        value: widget.t.category,
                      ),
                      myDetailRow(
                        title: "数额",
                        value: widget.t.amount.toString(),
                      ),
                      myDetailRow(
                        title: "日期",
                        value: widget.t.year.toString() + "-" + widget.t.month.toString() + "-" + widget.t.date.toString(),
                      ),
                      myDetailRow(
                        title: "说明",
                        value: widget.t.memo,
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget myDetailRow({String title, String value}) {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: myTitle(title),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: Text(
                value,
                style: TextStyle(
                  color: kBackColor.withOpacity(0.9),
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget myTitle(String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.grey.shade700.withOpacity(0.6),
          fontWeight: FontWeight.w600,
          fontSize: 14.0,
        ),
      ),
    );
  }

  showDeleteDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Text(
              "Delete",
              style: TextStyle(
                color: kBackColor,
                fontWeight: FontWeight.w600,
              ),
            ),
            content: Text("你是否确定要删除此项？"),
            contentTextStyle: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontSize: 16.0,
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  "取消",
                  style: TextStyle(
                    color: kBackColor,
                  ),
                ),
              ),
              TextButton(
                onPressed: () async {
                  var res = await dbh.deleteTransaction(widget.t.id);
                  if (res > 0) {
                    List responses =
                        await dbh.getMainData(widget.t.month, widget.t.year);
                    double finalIncome = widget.t.category == "收入"
                        ? responses[0].income - widget.t.amount
                        : responses[0].income;
                    double finalExpense = widget.t.category == "支出"
                        ? responses[0].expense - widget.t.amount
                        : responses[0].expense;
                    double finalBalance = finalIncome - finalExpense;
                    MainData m = MainData(
                        income: finalIncome,
                        expense: finalExpense,
                        balance: finalBalance,
                        month: widget.t.month,
                        year: widget.t.year);
                    var isUpdated = await dbh.updateMainData(m);
                    if (isUpdated > 0) {
                      widget.refresh();
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    } else {
                      print("未更新");
                    }
                  } else {
                    print("未删除");
                  }
                },
                child: Text(
                  "确定",
                  style: TextStyle(
                    color: kBackColor,
                  ),
                ),
              ),
            ],
          );
        });
  }
}
